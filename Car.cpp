#include "Car.h"
#include <sstream>
#include <iomanip>

using namespace std;

string Car::toString() const
{
    stringstream sstr;

    sstr << "VIN: " << _vin << endl
         << "Make: " << _make << endl
         << "Model: " << _model << endl
         << "Model Year: " << _modelYear << endl
         << setprecision(2) << fixed << showpoint
         << "Book Price: $" << _bookPrice << endl
         << "Needs Waxing: " << (_needsWaxing ? "Yes" : "No") << endl;

    return sstr.str();
}

string Car::marshal() const
{
    stringstream sstr;

    sstr << _vin << "," << _make << "," << _model << "," << _modelYear << ","
        << _bookPrice << "," << _needsWaxing;

    return sstr.str();
}

//Very bad error checking right now, I wrote this hastily
Car Car::unmarshal(string carStr)
{
    vector<string> data;
    Car newCar;
    split(carStr, ',', data);

    //Check that data has an element for each property
    if (data.size() != 6)
    {
        newCar._badUnmarshal = true;
    } else
    {
        newCar._vin = data[0];
        newCar._make = data[1];
        newCar._model = data[2];
        newCar._modelYear = stoi(data[3], nullptr);
        newCar._bookPrice = stod(data[4], nullptr);
        newCar._needsWaxing = stoi(data[5], nullptr);
    }

    return newCar;
}

//Borrowed this from http://stackoverflow.com/questions/236129/split-a-string-in-c
vector<string> &split(const string &s, char delim, vector<string> &elems)
{
    std::stringstream ss(s);
    std::string item;

    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }

    return elems;
}
