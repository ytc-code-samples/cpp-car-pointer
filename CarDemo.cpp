#include "Car.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cctype>
#include <limits>
#include <algorithm>
#include <string>

using namespace std;

//const int ARRAY_SIZE = 3;

//Primary subprocedures
//int mainMenu();
void carsFromUser();
void carsFromFile();
void programExitMessage();

//Helper functions
bool getCarInteractive(Car&);
Car getSingleCar();
void clearInput();
bool yesNoPrompt(string, string);
int integerPrompt(string, string);
double doublePrompt(string, string);
string stringPrompt(string, string);
//void sortVector(vector<Car>&);

//functor
struct sortByVin
{
    bool operator() (Car const& lhs, Car const& rhs) { return lhs.getVin() < rhs.getVin(); }
};

int main()
{
    while (true)
    {
        switch (integerPrompt("1) Enter cars and save to file\n2) Read cars from a file and display them\n0) Exit Program\n\nPlease select an option: ",
                              "Please enter a number."))
        {
        case 0:
            programExitMessage();
            return 0;
        case 1:
            carsFromUser();
            break;
        case 2:
            carsFromFile();
            break;
        default:
            continue;
        }
    }
}

//Main subprograms
void carsFromUser()
{
    vector<Car> vec;

    Car c;

    while (getCarInteractive(c))
        vec.push_back(c);

    //sort the vector
    sort(vec.begin(), vec.end(), sortByVin());

    //write the vector to a file
    string filename = stringPrompt("Please enter the name of a file to save the car list to: ", "Please enter a value.");
    ofstream out;
    out.open(filename);
    for (auto car : vec)
    {
        out << car.marshal() << endl;
    }

    out.close();
}

void carsFromFile()
{
    vector<Car> vec;

    string filename = stringPrompt("Please enter the name of a file to read cars from: ", "Please enter a value.");
    ifstream in;

    in.open(filename);

    string input;

    while (getline(cin, input))
    {
        if (!input.empty())
            vec.push_back(Car::unmarshal(input));
    }

    in.close();

    for (auto car : vec)
    {
        cout << car.toString() << endl;
    }
}

void programExitMessage()
{
    cout << "Now exiting..." << endl;
}

//Helper functions
bool getCarInteractive(Car& c)
{
    if (yesNoPrompt("Would you like to add a car to your list (Y/N)? ", "Please begin your response with 'Y' or 'N'"))
    {
        c = getSingleCar();
        return true;
    } else
    {
        return false;
    }
}

Car getSingleCar()
{
    //Vin
    string vin = stringPrompt("Enter the vehicle's VIN: ", "Please enter a value.");
    //Make
    string make = stringPrompt("Enter the vehicle's make: ", "Please enter a value.");
    //Model
    string model = stringPrompt("Enter the vehicle's model: ", "Please enter a value.");
    //Year
    int year = integerPrompt("Enter the vehicle's model year: ", "Please enter a whole number.");
    //Price
    double price = doublePrompt("Enter the vehicle's book price: ", "Please enter a number.");
    //Needs waxing?
    bool needsWax = yesNoPrompt("Does the vehicle need waxing (Y/N)? ", "Please begin your response with 'Y' or 'N'");

    return Car(vin, make, model, year, price, needsWax);
}

void clearInput()
{
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

bool yesNoPrompt(string prompt, string errMessage)
{
    string input;

    do
    {
        //If there was an input error, clear the error bits and flush the stream
        if (cin.fail())
        {
            clearInput();
            cout << errMessage << endl;
        }

        cout << prompt;

        getline(cin, input);
    } while (input.empty() || !(toupper(input.front()) == 'Y' || toupper(input.front()) == 'N'));

    switch (toupper(input.front()))
    {
    case 'Y':
        return true;
    default:
        return false;
    }
}

//I looked into making this a template but I think it's a bit too much for right now
int integerPrompt(string prompt, string errMessage)
{
    int input;

    do
    {
        //If there was an input error, clear the error bits and flush the stream
        if (cin.fail())
        {
            clearInput();
            cout << errMessage << endl;
        }

        cout << prompt;
    } while (!(cin >> input));

    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    return input;
}

double doublePrompt(string prompt, string errMessage)
{
    double input;

    do
    {
        //If there was an input error, clear the error bits and flush the stream
        if (cin.fail())
        {
            clearInput();
            cout << errMessage << endl;
        }

        cout << prompt;
    } while (!(cin >> input));

    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    return input;
}

string stringPrompt(string prompt, string errMessage)
{
    string input;
    bool err = false;

    do
    {
        //If there was an input error somehow, clear the error bits and flush the stream
        if (cin.fail())
            clearInput();

        if (err)
            cout << errMessage << endl;

        cout << prompt;
        getline(cin, input);
    } while (input.empty());

    return input;
}

//fix this later
/*
void sortVector(vector<Car>& cars)
{
    vector<Car> sorted;

    for (auto c : cars)
    {
        if (sorted.empty())
        {
            sorted.push_back(c);
        } else
        {
            for (auto it = sorted.begin(); it != sorted.empty(); ++it)
            {
                if ()
            }
            auto it = sorted.begin();

        }
    }
}
*/

//int main()
//{
//    Car cars[ARRAY_SIZE];
//
//    cars[0].setVin("1234567890");
//    cars[0].setMake("Dodge");
//    cars[0].setModel("Viper");
//    cars[0].setModelYear(2014);
//    cars[0].setBookPrice(87895);
//    cars[0].setNeedsWaxing(false);
//    cars[0].setNext(&cars[1]);
//
//    cars[1].setVin("0987654321");
//    cars[1].setMake("Chevy");
//    cars[1].setModel("Corvette");
//    cars[1].setModelYear(2012);
//    cars[1].setBookPrice(64000);
//    cars[1].setNeedsWaxing(false);
//    cars[1].setNext(&cars[2]);
//
//    cars[2].setVin("0192837465");
//    cars[2].setMake("Ford");
//    cars[2].setModel("Pinto");
//    cars[2].setModelYear(1988);
//    cars[2].setBookPrice(200);
//    cars[2].setNeedsWaxing(true);
//    cars[2].setNext(nullptr);   //In VS 2012, use 0 instead of nullptr
//
//    int counter = 0;
//    for (Car* carPtr = &cars[0]; carPtr != nullptr; carPtr = carPtr->getNext())
//    {
//        cout << "Car "<< ++counter << ":\n\n" << carPtr->toString() << endl;
//    }
//
//    ofstream carFile;
//
//    carFile.open("cardata.txt");
//
//    for (int i = 0; i < ARRAY_SIZE; ++i)
//    {
//        carFile << cars[i].marshal() << endl;
//    }
//
//    carFile.close();
//
//    ifstream carFileIn;
//    string carString;
//
//    carFileIn.open("cardata.txt");
//
//    getline(carFileIn, carString);
//
//    Car newCar = Car::unmarshal(carString);
//
//    cout << newCar.toString() << endl;
//    /*
//    for (int i = 0; i < ARRAY_SIZE; ++i)
//    {
//        cout << "Car "<< i + 1 << ":\n\n" << cars[i].toString() << endl;
//    }
//    */
//    return 0;
//}
