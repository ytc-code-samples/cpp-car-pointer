#pragma once

#include <string>
#include <vector>

class Car
{
private:
    std::string _vin;
    std::string _make;
    std::string _model;
    int _modelYear;
    double _bookPrice;
    bool _needsWaxing;
    bool _badUnmarshal = false;

public:
    //ctors
    Car() = default;
    Car(Car&) = default;
    Car(const Car&) = default;
    Car& operator=(const Car&) = default;
    Car(Car&&) = default;
    Car& operator=(Car&&) = default;
    ~Car() = default;

    Car(std::string vin, std::string make, std::string model, int modelYear, double bookPrice, bool waxing)
    {
        _vin = vin;
        _make = make;
        _model = model;
        _modelYear = modelYear;
        _bookPrice = bookPrice;
        _needsWaxing = waxing;
    }

    std::string getVin() const { return _vin; }
    void setVin(std::string str) { _vin = str; }

    std::string getMake() const { return _make; }
    void setMake(std::string str) { _make = str; }

    std::string getModel() const { return _model; }
    void setModel(std::string str) { _model = str; }

    int getModelYear() const { return _modelYear; }
    void setModelYear(int i) { _modelYear = i; }

    double getBookPrice() const { return _bookPrice; }
    void setBookPrice(double d) { _bookPrice = d; }

    bool needsWaxing() const { return _needsWaxing; }
    void setNeedsWaxing(bool val) { _needsWaxing = val; }

    std::string toString() const;

    std::string marshal() const;
    bool unmarshalFailure() const { return _badUnmarshal; }

    static Car unmarshal(std::string carStr);
};

//Helper function
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
